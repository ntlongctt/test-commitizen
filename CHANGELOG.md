# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2022-12-27)


### ⚠ BREAKING CHANGES

* **add new file:** test new file

### Features

* **add new file:** this is a testing commit for create new file ([51db9ba](https://gitlab.com/ntlongctt/test-commitizen/commit/51db9babe3d0be3c16f7a23c20108bc2002b6e29))
* **add version config:** add version config ([612c652](https://gitlab.com/ntlongctt/test-commitizen/commit/612c652f375b4e83c238891e1e33cc0901959f16))
